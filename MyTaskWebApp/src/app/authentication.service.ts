import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {JwtHelper} from "angular2-jwt";

@Injectable()
export class AuthenticationService {

  private host:string = "http://localhost:8080";
  private jwtToken:string;
  private roles:Array<any>;

  constructor(private http:HttpClient) { }

  login(user){
    // option observe response pour ne pas transfere à json, laisse l'objet comme il est
    return this.http.post(this.host+"/login",user, {observe: "response"});
  }

  logout(){
    this.jwtToken=null;
    localStorage.removeItem("token");
  }

  saveToken(jwt:string){
    this.jwtToken=jwt;
    localStorage.setItem("token",jwt);
    let jwtHelper= new JwtHelper();
    this.roles=jwtHelper.decodeToken(this.jwtToken).roles;
  }

  isAdmin(){
    for(let r of this.roles)
      if (r.authority=="ADMIN") return true;
    return false;
  }

  loadToken(){
    this.jwtToken=localStorage.getItem("token");
  }

  getTasks(){
    if(this.jwtToken==null) this.loadToken();
    //get les données au format json, car on a pas ajouter observe
    return this.http.get(this.host+"/tasks",
      {headers: new HttpHeaders({"Authorization":this.jwtToken})})
  }

  saveTask(task){
    // let headers=new HttpHeaders();
    // headers.append("Authorization",this.jwtToken);
    return this.http.post(this.host+"/tasks"
      , task
      , {headers: new HttpHeaders({"Authorization":this.jwtToken})})
  }

}
