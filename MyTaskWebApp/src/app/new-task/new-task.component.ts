import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../authentication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.css']
})
export class NewTaskComponent implements OnInit {

  mode=1;
  task:any;
  constructor(private authService:AuthenticationService,
              private  router:Router) { }

  ngOnInit() {
  }

  onSaveTask(task){
    this.authService.saveTask(task)
      .subscribe(resp=>{
        this.task=resp;
        // this.mode=2;
        this.router.navigateByUrl("/tasks")

      }, err=>{
        console.log(err);
        // this.mode=0;
      })


  }

}
